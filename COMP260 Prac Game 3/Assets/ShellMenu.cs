﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;
	private bool paused = true;

	void Start () {
		//options panel is initially hidden
		optionsPanel.SetActive (false);
		SetPaused (paused);

		//restore the saved audio volume
		if (PlayerPrefs.HasKey ("AudioVolume")) {
			AudioListener.volume =
				PlayerPrefs.GetFloat ("AudioVolume");
		} else {
			//first time the game is run, use the default value
			AudioListener.volume = 1;
		}

		//populate the list of avaiable resolutions
		resolutionDropdown.ClearOptions();
		List<string> resolutions = new List<string> ();
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add(Screen.resolutions[i].ToString());
		}
		resolutionDropdown.AddOptions(resolutions);

		//populate the list of video quality levels
		qualityDropdown.ClearOptions();
		List<string> names = new List<string> ();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add(QualitySettings.names [i]);
		}
		qualityDropdown.AddOptions (names);
	}

	public void OnPressedOptions () {
		//show the options panel and hide the shell panel
		shellPanel.SetActive(false);
		optionsPanel.SetActive (true);

		//set the volume slider
		volumeSlider.value = AudioListener.volume;
		PlayerPrefs.SetFloat ("AudioVolume", AudioListener.volume);
		//set the fullscreen toggle
		fullscreenToggle.isOn = Screen.fullScreen;

		//select the current quality value
		qualityDropdown.value = QualitySettings.GetQualityLevel();
		//select the current resolution
		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions[i].width == Screen.width &&
			Screen.resolutions[i].height == Screen.height) {
				currentResolution = i;
				break;
			}
		}
		resolutionDropdown.value = currentResolution;
	}

	public void OnPressedCancel() {
		//return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive (false);
	}

	public void OnPressedApply (){
		//apply the changes
		QualitySettings.SetQualityLevel(qualityDropdown.value);
		Resolution res =
			Screen.resolutions [resolutionDropdown.value];
		Screen.SetResolution (res.width, res.height, true);
		Screen.SetResolution (res.width, res.height, fullscreenToggle.isOn);
		AudioListener.volume = volumeSlider.value;
		//return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive (false);
	}

	void Update () {
		if (!paused && Input.GetKeyDown (KeyCode.Escape)) {
			SetPaused (true);
		}
	}
		private void SetPaused(bool p) {
			paused = p;
			shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
		}
	public void OnPressedPlay () {
		//resume the game
		SetPaused(false);
	}

	public void OnPressedQuit() {
		//quit the game
		Application.Quit();
	}

}
