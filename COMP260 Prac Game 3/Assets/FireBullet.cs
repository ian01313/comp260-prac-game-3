﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	void Start () {
	
	}

	public BulletMove bulletPrefab;
	public int maxNumberBullets = 10;
	public int numberBullets = 0;
	void Update () {
		//when the button is pushed, fire a bullet

		//do not run if the game is pausd
		if (Time.timeScale == 0) {
			return;
		}

		if (Input.GetButtonDown("Fire1")) {

			numberBullets++;
			BulletMove bullet = Instantiate (bulletPrefab);
			//the bullet starts at the player's position
			bullet.transform.position = transform.position;

			//create a ray towards the mouse location
			Ray ray =
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
		}
		if (numberBullets == maxNumberBullets) {

		}
	}

}